 var request = window.indexedDB.open("myDB", 1);
    request.onupgradeneeded = function (event) {
        var db = event.target.result;
        var alertAreasObjectStore, citiesDataObjectStore;
    
        //object store 1
    alertAreasObjectStore = db.createObjectStore("alertareas", { keyPath : "cityAlertId", unique: true });
        alertAreasObjectStore.createIndex("alertAreasAlertAreaIndex", "alertArea", { });
        alertAreasObjectStore.transaction.oncomplete = function(event) {
            var obs = db.transaction("alertareas", "readwrite").objectStore("alertareas");
            //loop - adding data
            obs.put(...);
            ... 
        }

        //object store 2
    citiesDataObjectStore = db.createObjectStore("cities", { autoIncrement: true });
        citiesDataObjectStore.createIndex("citiesAlertAreaIndex", "alertArea", { });
        citiesDataObjectStore.transaction.oncomplete = function(event) {
            var obs = db.transaction("cities", "readwrite").objectStore("cities");
            //loop - adding data
            obs.put(...);
            ... 
        }
    }